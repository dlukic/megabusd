# megabusclt

Megabusclt can handle multiple connections, subscriptions and actions.


## Usage
```
[lukicdarkoo@dlukic megabusd]$ ./megabusclt --config megabus.conf megabus_extra.conf --preview
.
├──stomp+ssl://agileinf-mb.cern.ch:61223 (x509 "/var/lib/puppet/ssl/certs/dlukic-dev2.cern.ch.pem")
│  └──/topic/roger.notification.hostgroup.playground [('subs_selector', "hostgroup like 'playground-%'")]
│     ├──command: echo "$(date)" >> /root/megabusclt.log
│     ├──command: echo "$(date)" >> /root/megabusclt2.log
│     └──command: /bin/stompclt_actions/ssl_agileinf-mb_61223-test
└──stomp+ssl://agileinf-mb.cern.ch:61223 (plain "myuser")
   ├──/topic/ccpco.notification [('subs_selector', "hostgroup like 'playground-%'"), ('subs_ack', True)]
   │  ├──receiver: /var/spool/receiver
   │  └──command: /bin/stompclt_actions/ssl_agileinf-mb_61223-test
   └──/topic/roger.notification.hostgroup.playground [('subs_selector', "hostgroup like 'playground-%'")]
      └──command: /bin/stompclt_actions/ssl_agileinf-mb_61223-test

Stastics:
You saved 4 (66.6%) connections
You saved 3 (50.0%) subscriptions
```

## TODO
[x] Add STOMP  
[x] Client side message filtering  
[ ] Add persistent as a parameter  
[ ] Add heart-beat parameter

## Backlog
[ ] Try to reuse connection by testing if authentification has privilegies  
[x] Handle sigint  
[ ] Design an interface for a message publishing

## Improvements compared to stompclt 
- Support for multiple cofiguration files
- Smart reuse of connections/subscriptions
- Configuration file(s) are easier to configure by Puppet
- Improved logging
- Support for command execution
- Support for an alias resolving
